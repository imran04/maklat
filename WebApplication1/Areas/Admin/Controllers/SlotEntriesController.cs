﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using WebApplication1.Data;
using WebApplication1.Models;

namespace WebApplication1.Areas.Admin.Controllers
{
    [Area("Admin")]
    public class SlotEntriesController : Controller
    {
        private readonly ApplicationDbContext _context;

        public SlotEntriesController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: Admin/SlotEntries
        public async Task<IActionResult> Index(DateTime? date)
        {
            date=date??DateTime.Now.Date;
            return View(await _context.SlotEntry.Where(i=>i.DateTime.Date==date.Value.Date).ToListAsync());
        }

        // GET: Admin/SlotEntries/Details/5
        public async Task<IActionResult> Details(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var slotEntry = await _context.SlotEntry
                .FirstOrDefaultAsync(m => m.Id == id);
            if (slotEntry == null)
            {
                return NotFound();
            }

            return View(slotEntry);
        }

        // GET: Admin/SlotEntries/Create
        public IActionResult Create()
        {
            return View();
        }


        public IActionResult createbulk()
        {
            return View();
        }

        [HttpPost]
        public IActionResult createbulk(SlotEntryViewModel model)
        {
            var slots = TimeZoneInfo.ConvertTime( new DateTime(model.Date.Year,model.Date.Month,model.Date.Day,10,0,0),TimeZoneInfo.FindSystemTimeZoneById("India Standard Time"));
            List<SlotEntry> entries = new List<SlotEntry>();
            for(int i=0;i<model.Ga.Length;i++){
                var entry = new SlotEntry();
               
                entry.DateTime = slots.AddMinutes(i*15);
                entry.Id = entry.DateTime.Ticks.ToString();
                entry.GoldNumber = model.Ga[i];
                entry.RajashriNumber = model.Ra[i];
                entry.ShubhlakshamiNumber = model.Su[i];
                entry.Star = model.St[i];
                entries.Add(entry);
                
            }
            _context.SlotEntry.AddRange(entries);
            _context.SaveChanges();
            return View(model);
        }
        // POST: Admin/SlotEntries/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,GoldNumber,ShubhlakshamiNumber,RajashriNumber,Star,DateTime,Slot")] SlotEntry slotEntry)
        {
            if (ModelState.IsValid)
            {
                _context.Add(slotEntry);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(slotEntry);
        }

        // GET: Admin/SlotEntries/Edit/5
        public async Task<IActionResult> Edit(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var slotEntry = await _context.SlotEntry.FindAsync(id);
            if (slotEntry == null)
            {
                return NotFound();
            }
            return View(slotEntry);
        }

        // POST: Admin/SlotEntries/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(string id, [Bind("Id,GoldNumber,ShubhlakshamiNumber,RajashriNumber,Star,DateTime,Slot")] SlotEntry slotEntry)
        {
            if (id != slotEntry.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(slotEntry);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!SlotEntryExists(slotEntry.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(slotEntry);
        }

        // GET: Admin/SlotEntries/Delete/5
        public async Task<IActionResult> Delete(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var slotEntry = await _context.SlotEntry
                .FirstOrDefaultAsync(m => m.Id == id);
            if (slotEntry == null)
            {
                return NotFound();
            }

            return View(slotEntry);
        }

        // POST: Admin/SlotEntries/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(string id)
        {
            var slotEntry = await _context.SlotEntry.FindAsync(id);
            _context.SlotEntry.Remove(slotEntry);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool SlotEntryExists(string id)
        {
            return _context.SlotEntry.Any(e => e.Id == id);
        }
    }
}
