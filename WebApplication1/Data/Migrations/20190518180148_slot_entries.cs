﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace WebApplication1.Data.Migrations
{
    public partial class slot_entries : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "SlotEntry",
                columns: table => new
                {
                    Id = table.Column<string>(nullable: false),
                    GoldNumber = table.Column<string>(nullable: true),
                    ShubhlakshamiNumber = table.Column<string>(nullable: true),
                    RajashriNumber = table.Column<string>(nullable: true),
                    Star = table.Column<string>(nullable: true),
                    DateTime = table.Column<DateTime>(nullable: false),
                    Slot = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SlotEntry", x => x.Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "SlotEntry");
        }
    }
}
