﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplication1.Models
{
    public class SlotEntry
    {
        public string Id { get; set; }

        public string GoldNumber { get; set; }

        public string ShubhlakshamiNumber { get; set; }

        public string RajashriNumber { get; set; }

        public string Star { get; set; }
        public DateTime DateTime { get; set; }
        public string Slot { get; set; }
    }
}
