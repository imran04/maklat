﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplication1.Models
{
    public class SlotEntryViewModel
    {
        public DateTime Date { get; set; }
        public string[] Ga { get; set; }
        public string[] Ra { get; set; }
        public string[] Su { get; set; }
        public string[] St { get; set; }
    }
}
